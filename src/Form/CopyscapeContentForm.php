<?php

namespace Drupal\copyscape\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Administrative form for Copyscape content settings route.
 */
class CopyscapeContentForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Constructs a new CopyscapeContentForm object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   */
  public function __construct(
    EntityFieldManager $entityFieldManager
  ) {
    $this->entityFieldManager = $entityFieldManager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'copyscape_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('copyscape.content');

    $form['reject_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reject content if copy percentage is bigger than (in %):'),
      '#required' => FALSE,
      '#default_value' => $config->get('reject_content'),
    ];

    $form['reject_value'] = [
      '#title' => $this->t(''),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $config->get('reject_value'),
    ];

    $form['site_ignore'] = [
      '#title' => $this->t('Ignore search from those sites'),
      '#description' => 'Ignore sites : Subdomains are also omitted from the results. For example, if set to site1.com,site2.com then www.site1.com and blog.site2.com would also be ignored. Ignore sites listed in your user settings are always applied.',
      '#type' => 'textfield',
      '#size' => 100,
      '#default_value' => $config->get('site_ignore'),
    ];

    foreach (node_type_get_names() as $machineName => $contentType) {
      $configName = "copyscape_ct.{$machineName}";
      $configNameForParaField = "copyscape_ct_para_field{$machineName}";
      $form[$configName] = [
        '#type' => 'fieldset',
        '#title' => $this->t($contentType),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];

      $fields = $this->entityFieldManager->getFieldDefinitions('node', $machineName);

      $allowedFields = [];
      foreach ($fields as $fieldMachineName => $field) {
        if (!$field instanceof FieldConfig) {
          continue;
        }

        if (!in_array($field->getType(), [
          'text_long',
          'string_long',
          'text_with_summary',
        ])) {
          continue;
        }

        $allowedFields[$fieldMachineName] = $field->getLabel();
      }

      if (empty($allowedFields)) {
        continue;
      }

      $form[$configName][$machineName] = [
        '#type' => 'checkboxes',
        '#options' => $allowedFields,
        '#default_value' => $config->get($configName) ? $config->get($configName) : [],
        '#weight' => 2,
      ];

      $form[$configName][$configNameForParaField] = [
        '#title' => $this->t('Paragraph field'),
        '#type' => 'textfield',
        '#description' => 'We can mention paragraph field here ex: [parent para]/[paragraph field name]:[long text field]:i.<br> [parent para] and ":i" are optional, [parent para] used for mentiont if there is any parent paragraph. :i used for check the paragraph field value as individualy.',
        '#size' => 40,
        '#default_value' => $config->get($configNameForParaField),
        '#weight' => 3,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $options = [];
    $this->config('copyscape.content')
      ->set('reject_content', $values['reject_content'])
      ->set('reject_value', $values['reject_value'])
      ->set('site_ignore', $values['site_ignore']);

    foreach (node_type_get_names() as $machineName => $contentType) {
      $configNameForParaField = "copyscape_ct_para_field{$machineName}";
      if (empty($values[$machineName])) {
        continue;
      }
      $options[$machineName] = $values[$machineName];
      $this->config('copyscape.content')
        ->set($configNameForParaField, $values[$configNameForParaField])
        ->save();
    }
    $this->config('copyscape.content')
      ->set('copyscape_ct', $options)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['copyscape.content'];
  }
}
